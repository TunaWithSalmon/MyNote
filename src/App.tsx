import * as React from 'react';
import { Provider } from 'react-redux'
import { Route, Switch} from 'react-router'
import { ConnectedRouter } from 'react-router-redux'
import { history , store} from './createStore'

import Home from './components/Home'

export default () => {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Switch>
          <Route path={'/'} component={Home} />
        </Switch>
      </ConnectedRouter>
    </Provider>
  )
}
