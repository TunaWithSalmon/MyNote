import * as React from 'react'
import { MobileNavbar } from '../components/MobileNavbar'
import { ILink } from '../types/shared'

interface IProps {
    links: ILink[]
}

interface IState {
    isOpen: boolean
}

export default class MobileNavbarContainer extends React.Component<IProps, IState> {
    public constructor(props: IProps) {
        super(props)

        this.state = {
            isOpen: false
        }
    }

    public openNavbar = (): void => this.setState({ isOpen: true })
    public closeNavbar = (): void => this.setState({ isOpen: false })

    public render(): JSX.Element {
        return <MobileNavbar
            links={this.props.links}
            isNavOpen={this.state.isOpen}
            onNavClose={this.closeNavbar}
            onNavOpen={this.openNavbar}
        />
    }
}