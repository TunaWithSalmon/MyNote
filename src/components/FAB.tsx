import * as React from 'react'

interface IProps {
    children: React.ReactNode    
}

export const FAB: React.StatelessComponent<IProps> = (props: IProps) => (
    <div>
       {props.children} 
    </div>
)