import * as React from 'react'
import '../css/Header.css'

interface IProps {
  title: string
  icon?: string
}

export const Header: React.StatelessComponent<IProps> = ({ title }) => (
  <div className={'Header'}>
    <div className={'title'}>{title}</div>
  </div>
)
