import * as React from 'react'
import { Link } from 'react-router-dom'
import { ILink } from '../types/shared'

interface IClosedProps {
    onOpen: () => void
}

const ClosedNavigation: React.StatelessComponent<IClosedProps> = (props: IClosedProps) => (
    <div onClick={props.onOpen} className={'material-icons'}>add</div>
)

interface IOpenedProps {
    links: ILink[]
    onClose: () => void
}

const OpenedNavigation: React.StatelessComponent<IOpenedProps> = (props: IOpenedProps) => (
    <div>
        <div className={'material-icons'} onClick={props.onClose} >clear</div>
        {props.links.map((link): JSX.Element => <Link to={link.path} key={link.path} className={'material-design'}>{link.icon}</Link>)}
    </div>
)


interface IFABNavigationProps {
    links: ILink[]
    onOpen: () => void
    onClose: () => void
    isOpen: boolean
}

export const FABNavigation: React.StatelessComponent<IFABNavigationProps> = (props: IFABNavigationProps) => (
    <div>
        {props.isOpen ? <OpenedNavigation onClose={props.onClose} links={props.links} /> : <ClosedNavigation onOpen={props.onOpen} />}
    </div>
)