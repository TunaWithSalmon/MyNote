import * as React from 'react'
import { Screen } from './Screen'

export default () => (
  <Screen isMobile={true}>Main page</Screen>
)
