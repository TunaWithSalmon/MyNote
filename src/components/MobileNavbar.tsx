import * as React from 'react'
import { ILink } from '../types/shared'

import { FABNavigation } from './FABNavigation'

export interface INavbarProps {
    links: ILink[]
    isNavOpen: boolean
    onNavOpen: () => void
    onNavClose: () => void
    leftButton?: JSX.Element
    rightButton?: JSX.Element
}

export const MobileNavbar: React.StatelessComponent<INavbarProps> = (props: INavbarProps) => (
    <div>
       {props.leftButton}
        <FABNavigation isOpen={props.isNavOpen}  onClose={props.onNavClose} onOpen={props.onNavOpen} links={props.links} />
       {props.rightButton}
    </div>
)
