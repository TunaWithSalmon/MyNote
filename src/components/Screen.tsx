import * as React from 'react'
import MobileNavbarContainer from '../containers/MobileNavbarContainer'
import '../css/Screen.css'
import { ILink } from '../types/shared'
import { Header } from './Header'

const links: ILink[] = [
  { icon: 'clear', path: '/' },
  { icon: 'clear', path: '/' },
  { icon: 'clear', path: '/' },
  { icon: 'clear', path: '/' }
]

interface IScreenInnerProps {
  children: React.ReactNode
}

export const DesktopScreen: React.StatelessComponent<IScreenInnerProps> = ({ children }) => (
  <div>
    j
  </div>
)

export const MobileScreen: React.StatelessComponent<IScreenInnerProps> = ({ children }) => (
  <div>
    <Header title={'Home'} />
    <MobileNavbarContainer links={links} />
  </div>
)

interface IScreenOuterProps {
  isMobile: boolean
  children: React.ReactNode
}

export const Screen: React.StatelessComponent<IScreenOuterProps> = ({ children, isMobile }): JSX.Element => (
  <div className={'Screen'}>
      {isMobile ? <MobileScreen children={children} /> : <DesktopScreen children={children} />}
  </div>
)
