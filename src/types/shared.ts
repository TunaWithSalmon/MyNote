export interface ILink {
    path: string
    icon: string
}