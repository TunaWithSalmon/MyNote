import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux'
import { reducer as form } from 'redux-form'
import { dbReducer } from './db'

export const makeRootReducer = () => {
    return combineReducers({
        db: dbReducer,
        form,
        router: routerReducer
    })
}

export default makeRootReducer
