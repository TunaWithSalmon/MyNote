import PouchDB from 'pouchdb'
import { Action, Reducer } from 'redux'

interface IDBReducerState {
  db: any
}

const initialState: IDBReducerState = {
  db: new PouchDB('notepeople')
}

export const dbReducer: Reducer<IDBReducerState> = (state: IDBReducerState = initialState, action: Action) => {
  switch (action.type) {
    default:
      return state
  }
}
