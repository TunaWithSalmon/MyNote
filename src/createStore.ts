import createHistory from 'history/createBrowserHistory'
import { routerMiddleware } from 'react-router-redux'
import { applyMiddleware, compose, createStore } from 'redux'
import thunk from 'redux-thunk'
import makeRootReducer from './reducers'

export const history = createHistory()
const historyMiddleware = routerMiddleware(history)

const storeFactory = (initialState = {}) => {
  const middleware = [
    historyMiddleware,
    thunk
  ]
  
  const storeResult = createStore(
    makeRootReducer(),
    initialState,
    compose(applyMiddleware(...middleware))
  )

  return storeResult
}

export const store = storeFactory()
